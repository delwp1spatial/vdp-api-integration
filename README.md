# VDP API Integration

This repo provides sample scripts for integration with the VDP API.The scripts do not provide guidance for every possible workflow, but are designed to act as a starting point for loaders looking to integrate with the VDP. The scripts are written in python, with comments provided for extra context. I hope that loaders using different languages will still find the scripts useful for their understanding.

## Installation/Setup
In order to run the scripts, `python` must be installed on the machine. Additionally, the `requests` package is used, the documentation for which is available [here](https://docs.python-requests.org/en/latest/). I used version `3.8` of python and `2.27.1` of requests for testing. Despite this, there shouldn't be any strict version requirements (except using **Python 3.6 or greater**).

## Running the scripts
There are two scripts provided:
- `ingest.py`
- `validate.py`

The scripts require several parameters to be supplied by the loader in the `constants.py` file, which is utilised by the other scripts.

These scripts support the workflow of a loader updating a dataset, that has already been loaded to the VDP:
- `ingest.py` should be run first. After it completes, the VDP will start ingesting the supplied file. Loaders can check the UI, or perform `GET /dataload/{id}` requests to check that the data has been `Ingested` before proceeding to the next step.
- `validate.py` should be run next. Once it completes, the VDP will be in the process of loading the data into the staging database. If the `ingest.py` is used as provided (with the destination set to `publish-stage`), the dataset will also be automatically progressed to publication.

## Further documentation
The scripts provided do not provide an exhaustive description of the VDP API functionality. Further information can be found at the following hosted documentation:

- [VDP API](https://y6bfir5jxf.execute-api.ap-southeast-2.amazonaws.com/uat/docs) - Describes the majority of the endpoints used during the load process
- [IDAM API](https://3di47q62f6.execute-api.ap-southeast-2.amazonaws.com/uat/docs) - Describes the authentication calls used


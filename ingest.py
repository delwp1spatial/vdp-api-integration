import requests

from constants import *

"""
Retrieve an identity token from IDAM. The user's email and password are supplied
as json in the request body. This endpoint returns an object containing an IdToken
and a Refresh token. It is possible to use the refresh token to get new IdTokens
without supplying the user's email and password in subsequent requests.
"""
body = {"user_email": USERNAME, "password": PASSWORD}
response = requests.post(f"{IDAM_URL}/access/create-token", json=body)
id_token = response.json()['IdToken']

auth_headers = {"Authorization": f"Bearer {id_token}"}

"""
Retrieve the dataset record from the VDP, by looking up with the Metashare UUID
"""
query_params = {
    "uuid": DATASET_UUID
}
response = requests.get(f"{VDP_URL}/dataset", params=query_params, headers=auth_headers)

try:
    dataset = response.json()[0]
except KeyError:
    raise Exception("Invalid UUID supplied. Has the dataset been loaded into the VDP yet?")

dataset_id = dataset["dataset_id"]

"""
Start the update of a dataset, by requesting an upload URL. The returned url is
pre-configured for the direct upload of the file to an AWS S3 bucket, with all
necessary authentication and configuration
"""
query_params = {
    "file": FILENAME
}
response = requests.get(f"{VDP_URL}/upload/{DATASET_UUID}", params=query_params, headers=auth_headers)
put_object_url = response.json()['url']
file_destination_key = response.json()['key']

"""
Upload the file using the returned url. Note that this call does NOT require
an Authorization header. This call is made directly to AWS, not the VDP API.

This operation is described in Amazon's docs:
https://docs.aws.amazon.com/AmazonS3/latest/API/API_PutObject.html
"""
with open(FILEPATH, mode="rb") as file:
    headers = {
        "Content-Type": ""
    }
    files = {
        "file": file
    }
    response = requests.put(put_object_url, headers=headers, files=files)

"""
This call completes the first step of the load process, triggering
the ingesting of the file into the VDP
"""
body = {
    "dataset_id": dataset_id,
    "destination": "publish-stage",
    "filetype": "FILEGDB",
    "load_method": "API",
    "load_type": "FULL",
    "s3_path": file_destination_key
}
response = requests.post(f"{VDP_URL}/dataload", headers=auth_headers, json=body)
pass

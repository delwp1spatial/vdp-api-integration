import requests

from constants import *

"""
Retrieve an identity token from IDAM. The user's email and password are supplied
as json in the request body. This endpoint returns an object containing an IdToken
and a Refresh token. It is possible to use the refresh token to get new IdTokens
without supplying the user's email and password in subsequent requests.
"""
body = {"user_email": USERNAME, "password": PASSWORD}
response = requests.post(f"{IDAM_URL}/access/create-token", json=body)
id_token = response.json()['IdToken']

auth_headers = {"Authorization": f"Bearer {id_token}"}

"""
Retrieve the dataset record from the VDP, by looking up with the Metashare UUID. From
the dataset record, we can also determine the current in-progress load
"""
query_params = {
    "uuid": DATASET_UUID
}
response = requests.get(f"{VDP_URL}/dataset", params=query_params, headers=auth_headers)

try:
    dataset = response.json()[0]
except KeyError:
    raise Exception("Invalid UUID supplied. Has the dataset been loaded into the VDP yet?")

dataset_id = dataset["dataset_id"]
current_load = dataset["last_load"]
current_load_id = current_load["id"]
current_load_status = current_load["status"]

"""
(Optional) A quick sanity check to make sure that the load is at the correct stage of the 
process. This is not strictly necessary, and can be removed if the user of this
script knows that the dataset is in the ingested step
"""
if not current_load_status == "schema-validated":
    raise Exception("Dataset needs to be `Ingested` before it can be validated")

"""
Retrieve the in-progress dataload record.
"""
response = requests.get(f"{VDP_URL}/dataload/{current_load_id}", headers=auth_headers)
load_schema = response.json()['load_schema']

"""
(Optional) Check that there have been no data-structure changes requiring change management.
This is not necessary if the user of the script is comfortable with the possibility
of change management being required.
"""
response = requests.post(f"{VDP_URL}/dataload/{current_load_id}/checkstructure", headers=auth_headers, json=load_schema)
structure_change = response.json()["change"]

if structure_change:
    raise Exception("The loaded data implies a change to the data structure, which may require change management")

"""
Update the dataload record with any changes to the load_schema. This allows for the user to:
- Unpublish fields
- Rename fields
- Change data types of fields
- Add or remove indexes

Even when no changes are being made, the user is still required to update the load with
the load_schema.
"""
body = {
    "load_schema": load_schema
}
requests.patch(f"{VDP_URL}/dataload/{current_load_id}", headers=auth_headers, json=body)

"""
This step triggers the load of data into the staging database. Since the destination
of the load was set to `publish-stage` (Publication Database) in the ingest stage, it will immediately
progress to publication once staging is complete.
"""
requests.patch(f"{VDP_URL}/dataload/{current_load_id}/stage", headers=auth_headers)
pass
